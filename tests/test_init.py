# test_transfer.py

import os
import pytest
import logging
from datetime import datetime
from pathlib import Path
from cheesefactory_logger_sqlite import CfLogSqlite

logger = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__))
db_path = f'{current_dir}/db.sqlite'


def test_cflogsqlite():
    """Are the defaults what we expect them to be?"""
    log = CfLogSqlite()
    assert log._connection is None
    assert log._cursor is None
    assert log.database_path == './log.sqlite'
    assert log.table == 'log'
    assert log.field_list == {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'message': 'TEXT',
        'timestamp': 'TEXT DEFAULT CURRENT_TIMESTAMP',
    }


#
# CLASS METHODS
#

def test_connect_newdb(caplog):
    caplog.set_level(logging.DEBUG)

    # Clean up
    Path(db_path).unlink(missing_ok=True)
    assert not Path(db_path).exists()

    field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
        'remote_host': 'TEXT',
        'remote_path': 'TEXT',
        'remove_source': 'INTEGER',
        'remove_source_ok': 'INTEGER',
        'size': 'INTEGER',
        'size_match_ok': 'INTEGER',
        'status': 'INTEGER',
        'suffix': 'TEXT',
        'suffix_ok': 'INTEGER',
        'timestamp': 'TEXT DEFAULT CURRENT_TIMESTAMP',
    }
    log = CfLogSqlite.connect(database_path=db_path, create=True, field_list=field_list)
    assert f'Connected to log database: {db_path}' in caplog.records[-1].msg

    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp', preserve_mtime=1,
        remote_host='172.16.1.1', remote_path='/upload', remove_source=1, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=1, remove_source_ok=1, size=2232, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=0
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp5', preserve_mtime=1,
        remote_host='172.16.1.1', remote_path='/upload5', remove_source=1, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=0, remove_source_ok=1, size=245, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=1
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp4', preserve_mtime=0,
        remote_host='172.16.1.1', remote_path='/upload4', remove_source=1, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=1, remove_source_ok=0, size=274, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=1
    )

    # Read and test to make sure things are good so far. The last field is the timestamp that cannot be matched, because
    # it changes every run. So, it is removed with [:1] for the comparison.
    results = log.read_records()
    assert results[0][:-1] == (1, 'GET', None, 'CfTester', '192.168.1.1', '/tmp', 'done', 1, 1, None, '172.16.1.1',
                               '/upload', 1, 1, 2232, None, 0, None, None)
    assert results[1][:-1] == (2, 'GET', None, 'CfTester', '192.168.1.1', '/tmp5', 'done', 1, 0, None, '172.16.1.1',
                               '/upload5', 1, 1, 245, None, 1, None, None)
    assert results[2][:-1] == (3, 'GET', None, 'CfTester', '192.168.1.1', '/tmp4', 'done', 0, 1, None, '172.16.1.1',
                               '/upload4', 1, 0, 274, None, 1, None, None)

    # Close and reconnect to make sure it continues to append
    log.close()
    log = CfLogSqlite.connect(database_path=db_path, create=True, field_list=field_list)
    assert f'Connected to log database: {db_path}' in caplog.records[-1].msg

    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp3', preserve_mtime=1,
        remote_host='172.16.1.1', remote_path='/upload3', remove_source=1, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=1, remove_source_ok=1, size=780567, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=0
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp2', preserve_mtime=1,
        remote_host='172.16.1.1', remote_path='/upload2', remove_source=0, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=1, remove_source_ok=1, size=35834562, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=1
    )
    pk = log.write_kwargs(
        action='GET', client='CfTester', local_host='192.168.1.1', local_path='/tmp1', preserve_mtime=0,
        remote_host='172.16.1.1', remote_path='/upload1', remove_source=1, status=0
    )
    log.write_kwargs(
        pk=pk,
        preserve_mtime_ok=0, remove_source_ok=1, size=34535, notes='not done yet'
    )
    log.write_kwargs(
        pk=pk,
        notes='done', status=0
    )

    results = log.read_records()
    assert results[0][:-1] == (1, 'GET', None, 'CfTester', '192.168.1.1', '/tmp', 'done', 1, 1, None, '172.16.1.1',
                               '/upload', 1, 1, 2232, None, 0, None, None)
    assert results[1][:-1] == (2, 'GET', None, 'CfTester', '192.168.1.1', '/tmp5', 'done', 1, 0, None, '172.16.1.1',
                               '/upload5', 1, 1, 245, None, 1, None, None)
    assert results[2][:-1] == (3, 'GET', None, 'CfTester', '192.168.1.1', '/tmp4', 'done', 0, 1, None, '172.16.1.1',
                               '/upload4', 1, 0, 274, None, 1, None, None)
    assert results[3][:-1] == (4, 'GET', None, 'CfTester', '192.168.1.1', '/tmp3', 'done', 1, 1, None, '172.16.1.1',
                               '/upload3', 1, 1, 780567, None, 0, None, None)
    assert results[4][:-1] == (5, 'GET', None, 'CfTester', '192.168.1.1', '/tmp2', 'done', 1, 1, None, '172.16.1.1',
                               '/upload2', 0, 1, 35834562, None, 1, None, None)
    assert results[5][:-1] == (6, 'GET', None, 'CfTester', '192.168.1.1', '/tmp1', 'done', 0, 0, None, '172.16.1.1',
                               '/upload1', 1, 1, 34535, None, 0, None, None)

    # Clean up
    log.close()
    Path(db_path).unlink()
    assert not Path(db_path).exists()


def test_connect_error_nodatabasepath():
    with pytest.raises(TypeError) as excinfo:
        # noinspection PyArgumentList
        CfLogSqlite.connect()
    assert str(excinfo.value) == "connect() missing 1 required positional argument: 'database_path'"


def test_connect_error_noexist_nocreate():
    with pytest.raises(FileNotFoundError) as excinfo:
        CfLogSqlite.connect(database_path=db_path)
    assert str(excinfo.value) == 'SQLite database does not exist: /opt/project/tests/db.sqlite'


def test_connect_error_nofieldlist():
    with pytest.raises(ValueError) as excinfo:
        CfLogSqlite.connect(database_path=db_path, create=True)
    assert str(excinfo.value) == 'Missing field_list.'


#
# PUBLIC METHODS
#

def test_archive(caplog):
    caplog.set_level(logging.DEBUG)
    log_path = Path(db_path)
    # What archive() should append, minus seconds
    append_date = datetime.strftime(datetime.now(), '%Y%m%d_%H%M')

    log_path.touch(exist_ok=True)  # Create something to move
    assert log_path.exists()

    log = CfLogSqlite()
    log.database_path = str(log_path)
    archived_log_path = log.archive()

    assert not log_path.exists()
    assert Path(archived_log_path).exists()
    assert f'Database file name changed: {log.database_path} -> {log.database_path}.{append_date}' \
           in caplog.records[-1].msg

    # Clean up
    Path(archived_log_path).unlink()
    assert not Path(archived_log_path).exists()


def test_move_log(caplog):
    caplog.set_level(logging.DEBUG)
    log_path = Path(db_path)
    new_log_path = Path(f'{current_dir}/db.sqlite.0')

    log_path.touch(exist_ok=True)  # Create something to move
    assert log_path.exists()

    log = CfLogSqlite()
    log.database_path = str(log_path)
    log.move_log(new_path=str(new_log_path))
    assert not log_path.exists()
    assert new_log_path.exists()
    assert f'Database file name changed: {log.database_path} -> {log.database_path}' in caplog.records[-1].msg

    # Clean up
    new_log_path.unlink()
    assert not new_log_path.exists()


def test_move_log_error_nodatabasepath():
    log = CfLogSqlite()
    log.database_path = None
    with pytest.raises(ValueError) as excinfo:
        log.move_log(new_path=f'{current_dir}/db.sqlite.0')
    assert str(excinfo.value) == 'No database_path defined.'


def test_move_log_error_nonewpath():
    log = CfLogSqlite()
    with pytest.raises(ValueError) as excinfo:
        log.move_log(new_path=None)
    assert str(excinfo.value) == 'No new_path defined. Nothing to move to!'
