# test_model.py

import logging
import os
import sqlite3
from pathlib import Path
import pytest
from cheesefactory_logger_sqlite.exceptions import MissingTableError, TablePrimaryKeyError
from cheesefactory_logger_sqlite.model import CfLogSqliteModel

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__))
db_path = f'{current_dir}/db.sqlite'


def test_cflogsqliteconnection():
    """Are attributes at expected values?"""
    model = CfLogSqliteModel()
    assert model._connection is None
    assert model._cursor is None
    assert model.database_path == './log.sqlite'
    assert model.table == 'log'
    assert model.field_list == {}


#
# PROPERTIES
#

def test_pk_fields():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    assert model.pk_field == 'id'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT PRIMARY KEY',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    assert model.pk_field == 'local_host'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    assert model.pk_field == ''


#
# PROTECTED METHODS
#

def test_create_table():
    """Does a table get properly created?"""
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    result = model.execute(f'PRAGMA table_info({model.table})')

    assert result == [
        (0, 'id', 'INTEGER', 0, None, 1),
        (1, 'action', 'TEXT', 0, None, 0),
        (2, 'action_ok', 'INTEGER', 0, None, 0),
        (3, 'client', 'TEXT', 0, None, 0),
        (4, 'local_host', 'TEXT', 0, None, 0),
        (5, 'local_path', 'TEXT', 0, None, 0),
        (6, 'notes', 'TEXT', 0, None, 0),
        (7, 'preserve_mtime', 'INTEGER', 0, None, 0),
        (8, 'preserve_mtime_ok', 'INTEGER', 0, None, 0),
        (9, 'redo', 'INTEGER', 0, None, 0),
    ]

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_create_table_noconnection():
    """Will an error raise if no connection exists when creating table?"""
    model = CfLogSqliteModel()
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
    }
    with pytest.raises(AttributeError) as excinfo:
        model._create_table()
    assert 'No SQLite connection exists. Connect before creating table.' in str(excinfo.value)


def test_create_table_nopk(caplog):
    """Does a table get properly created?"""
    caplog.set_level(logging.WARNING)

    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    result = model.execute(f'PRAGMA table_info({model.table})')

    assert result == [
        (0, 'id', 'INTEGER', 0, None, 0),
        (1, 'action', 'TEXT', 0, None, 0),
        (2, 'action_ok', 'INTEGER', 0, None, 0),
        (3, 'client', 'TEXT', 0, None, 0),
        (4, 'local_host', 'TEXT', 0, None, 0),
        (5, 'local_path', 'TEXT', 0, None, 0),
        (6, 'notes', 'TEXT', 0, None, 0),
        (7, 'preserve_mtime', 'INTEGER', 0, None, 0),
        (8, 'preserve_mtime_ok', 'INTEGER', 0, None, 0),
        (9, 'redo', 'INTEGER', 0, None, 0)
    ]
    assert caplog.records[-1].msg == f'Table (mytable) has no primary key. UPDATEs are not possible.'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_test_database(caplog):
    caplog.set_level(logging.DEBUG)
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
        'preserve_mtime': 'INTEGER',
        'preserve_mtime_ok': 'INTEGER',
        'redo': 'INTEGER',
    }
    model._create_table()
    model._test_database()
    assert caplog.records[-1].msg == f'Table successfully tested: mytable'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_test_database_missing_fields():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
        'notes': 'TEXT',
    }

    with pytest.raises(sqlite3.Error) as excinfo:
        model._test_database()
    assert str(excinfo.value) == f'Table {model.table} is missing required fields: notes'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_test_database_missingtable():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._connect(str(database_path))
    assert model.status == (True, None)

    model.table = 'mytable'
    with pytest.raises(MissingTableError) as excinfo:
        model._test_database()
    assert str(excinfo.value) == f'Table (mytable) missing in database ({db_path})'

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


#
# PUBLIC METHODS
#

def test_dump_table():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here2.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=1, client='client3', local_host='192.10.70.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.60.4',
                       local_path='/tmp/here5.txt')
    model.write_kwargs(action='GET', action_ok=0, client='client4', local_host='192.10.50.4',
                       local_path='/tmp/here4.txt')

    result = model.read_records()

    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt'),
        (6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt'),
        (7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt')
    ]

    dump = model.dump_table(exclude_pk=False)
    assert dump == "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt');\n"

    dump = model.dump_table(exclude_pk=False, where='action_ok = 0')
    assert dump == "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt');\n"

    dump = model.dump_table(exclude_pk=False, target_table='new_table')
    assert dump == "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt');\n"

    dump = model.dump_table(exclude_pk=False, where='action_ok = 0', target_table='new_table')
    assert dump == "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO new_table (id, action, action_ok, client, local_host, local_path) VALUES " \
                   "(7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt');\n"

    dump = model.dump_table(exclude_pk=True)
    assert dump == "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt');\n" \
                   "INSERT INTO mytable (action, action_ok, client, local_host, local_path) VALUES " \
                   "('GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt');\n"

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_exists():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here2.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=1, client='client3', local_host='192.10.70.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.60.4',
                       local_path='/tmp/here5.txt')
    model.write_kwargs(action='GET', action_ok=0, client='client4', local_host='192.10.50.4',
                       local_path='/tmp/here4.txt')

    result = model.read_records()

    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt'),
        (6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt'),
        (7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt')
    ]

    exists = model.exists()
    assert exists is True

    exists = model.exists(where='action_ok = 0')
    assert exists is True

    exists = model.exists(where="local_host LIKE '192.10.60%'")
    assert exists is True

    exists = model.exists(where="local_host = 'nonexistent'")
    assert exists is False

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_read_records():
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here2.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='GET', action_ok=1, client='client3', local_host='192.10.70.4',
                       local_path='/tmp/here3.txt')
    model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.60.4',
                       local_path='/tmp/here5.txt')
    model.write_kwargs(action='GET', action_ok=0, client='client4', local_host='192.10.50.4',
                       local_path='/tmp/here4.txt')

    result = model.read_records()

    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (4, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt'),
        (5, 'GET', 1, 'client3', '192.10.70.4', '/tmp/here3.txt'),
        (6, 'PUT', 1, 'test run', '192.10.60.4', '/tmp/here5.txt'),
        (7, 'GET', 0, 'client4', '192.10.50.4', '/tmp/here4.txt')
    ]

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_write_kwargs_insert(caplog):
    caplog.set_level(logging.DEBUG)
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    pk = model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');"
    assert pk == 1

    pk = model.write_kwargs(action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here2.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt');"
    assert pk == 2

    pk = model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here3.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');"
    assert pk == 3

    result = model.execute(f'SELECT * from {model.table};')
    assert result == [
        (1, 'GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt'),
        (2, 'PUT', 1, 'test run', '192.10.10.4', '/tmp/here2.txt'),
        (3, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt')
    ]

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_write_kwargs_invalidkwargs():
    """Is an error raised if kwargs have invalid keys?"""
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()
    with pytest.raises(ValueError) as excinfo:
        model.write_kwargs(bad_key='this is not good.')
    assert str(excinfo.value) == f'Invalid key(s): bad_key. Acceptable values are id, action, action_ok, client, ' \
                                 f'local_host, local_path'


def test_write_kwargs_missingkwargs():
    """Is an error raised if missing kwargs?"""
    model = CfLogSqliteModel()
    with pytest.raises(ValueError) as excinfo:
        model.write_kwargs()
    assert str(excinfo.value) == f'Missing kwargs. Cannot write to database.'


def test_write_kwargs_update(caplog):
    caplog.set_level(logging.DEBUG)
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER PRIMARY KEY AUTOINCREMENT',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()

    pk = model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');"
    assert pk == 1

    pk = model.write_kwargs(pk=1, action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here2.txt')
    assert caplog.records[-1].msg == "write(): UPDATE mytable SET action = 'PUT', action_ok = 1, " \
                                     "client = 'test run', local_host = '192.10.10.4', local_path = '/tmp/here2.txt' " \
                                     "WHERE id = 1;"
    assert pk == 1

    pk = model.write_kwargs(pk=1, action_ok=1, client='bad bob', local_path='/tmp/here32.txt')
    assert caplog.records[-1].msg == "write(): UPDATE mytable SET action_ok = 1, client = 'bad bob', " \
                                     "local_path = '/tmp/here32.txt' WHERE id = 1;"
    assert pk == 1

    pk = model.write_kwargs(action='GET', action_ok=0, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here3.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt');"
    assert pk == 2

    result = model.execute(f'SELECT * from {model.table};')
    assert result == [
        (1, 'PUT', 1, 'bad bob', '192.10.10.4', '/tmp/here32.txt'),
        (2, 'GET', 0, 'test run', '192.10.10.4', '/tmp/here3.txt')
    ]

    # Clean up
    model.close()
    database_path.unlink()
    assert not database_path.exists()


def test_write_kwargs_update_nopk(caplog):
    caplog.set_level(logging.DEBUG)
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    model = CfLogSqliteModel()
    model._connect(str(database_path))
    model.table = 'mytable'
    model.field_list = {
        'id': 'INTEGER',
        'action': 'TEXT',
        'action_ok': 'INTEGER',
        'client': 'TEXT',
        'local_host': 'TEXT',
        'local_path': 'TEXT',
    }
    model._create_table()
    assert caplog.records[-1].msg == 'Table (mytable) has no primary key. UPDATEs are not possible.'

    pk = model.write_kwargs(action='GET', action_ok=1, client='test run', local_host='192.10.10.4',
                            local_path='/tmp/here.txt')
    assert caplog.records[-1].msg == "write(): INSERT INTO mytable (action, action_ok, client, local_host, " \
                                     "local_path) VALUES ('GET', 1, 'test run', '192.10.10.4', '/tmp/here.txt');"
    assert pk == 1

    with pytest.raises(TablePrimaryKeyError) as excinfo:
        model.write_kwargs(pk=1, action='PUT', action_ok=1, client='test run', local_host='192.10.10.4',
                                local_path='/tmp/here2.txt')
    assert str(excinfo.value) == f'Table {model.database_path}.{model.table} Error: The table has no primary key. ' \
                                 f'SQL UPDATE cannot happen.'
    assert caplog.records[-1].msg == 'No primary key found.'
