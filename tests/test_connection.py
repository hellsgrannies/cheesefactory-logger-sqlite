# test_connection.py

import logging
import os
import re
from pathlib import Path
import pytest
from cheesefactory_logger_sqlite.connection import CfLogSqliteConnection

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__))
db_path = f'{current_dir}/db.sqlite'


def test_cflogsqliteconnection():
    """Are attributes at expected values?"""
    connection = CfLogSqliteConnection()
    assert connection._connection is None
    assert connection._cursor is None
    assert connection.database_path == './log.sqlite'


#
# PROPERTIES
#

def test_status_no_connection():
    """Do we get an expected status when no connection is present?"""
    connection = CfLogSqliteConnection()
    assert (False, 'No connection detected.') == connection.status


#
# PROTECTED METHODS
#

def test_connect_existing(caplog):
    """Does it connect as expected to an existing database file?"""
    caplog.set_level(logging.DEBUG)

    # Make sure the file exists
    database_path = Path(db_path)
    database_path.touch()
    assert database_path.exists()

    connection = CfLogSqliteConnection()
    connection._connect(database_path=str(database_path), create=False)
    assert connection.status == (True, None)
    assert f'Connecting to existing database: {database_path}' == caplog.records[-1].msg

    # Clean up
    connection.close()
    database_path.unlink()
    assert not database_path.exists()


def test_connect_nonexist_create(caplog):
    """Is a new database file created, if needed?"""
    caplog.set_level(logging.DEBUG)

    # Make sure the file doesn't exist
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not database_path.exists()

    connection = CfLogSqliteConnection()
    connection._connect(database_path=str(database_path), create=True)
    assert connection.status == (True, None)
    assert database_path.exists()
    assert f'Connecting to new database: {database_path}' == caplog.records[-1].msg

    # Clean up
    connection.close()
    database_path.unlink()
    assert not database_path.exists()


def test_connect_nonexist_nocreate():
    """Is a error raised when file doesn't exist and creation is disallowed?"""
    # Make sure the file doesn't exist
    database_path = Path(db_path)
    database_path.unlink(missing_ok=True)
    assert not Path(database_path).exists()

    with pytest.raises(FileNotFoundError) as excinfo:
        connection = CfLogSqliteConnection()
        connection._connect(database_path=str(database_path), create=False)
    assert str(excinfo.value) == f'SQLite database does not exist: {database_path}'


#
# PUBLIC METHODS
#

def test_close():
    """Does the connection close?"""
    # Make sure the file exists
    database_path = Path(db_path)
    database_path.touch()
    assert database_path.exists()

    connection = CfLogSqliteConnection()
    connection._connect(database_path=str(database_path), create=False)
    assert connection.status == (True, None)
    connection.close()
    assert connection.status == (False, 'No connection detected.')

    # Clean up
    database_path.unlink()
    assert not database_path.exists()


def test_execute():
    """Can the connection be used to execute queries?"""
    # Make sure the file exists
    database_path = Path(db_path)
    database_path.touch()
    assert database_path.exists()

    connection = CfLogSqliteConnection()
    connection._connect(database_path=str(database_path), create=False)
    assert connection.status == (True, None)
    result = connection.execute('SELECT sqlite_version();')
    sqlite_version = result[0][0]
    assert re.search(r'^[0-9]+\.[0-9]+\.[0-9]+$', sqlite_version)

    # Clean up
    connection.close()
    database_path.unlink()
    assert not database_path.exists()
